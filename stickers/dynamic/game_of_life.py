#!/bin/python

import numpy as np

MAX_ITER = 100
RESTART_DELAY = 3
POP_DENSITY = 0.25

# Original script from https://jakevdp.github.io/blog/2013/08/07/conways-game-of-life/

def life_step(X):
    """Game of life step using generator expressions"""
    nbrs_count = sum(np.roll(np.roll(X, i, 0), j, 1)
                     for i in (-1, 0, 1) for j in (-1, 0, 1)
                     if (i != 0 or j != 0))
    return (nbrs_count == 3) | (X & (nbrs_count == 2))


def print_board(board):
    """Print board state in sticker format"""
    print("frame")
    for row in board:
        row_total = 0
        for column, state in enumerate(row):
            row_total += int(state)*2**(column)
        print(f"x{row_total:02x}")
    print("update")


def init_board():
    board = np.zeros((8, 8), dtype=bool)
    r = np.random.random((8, 8))
    board = (r < POP_DENSITY)
    return board


while True:
    iter_counter = 0
    board = init_board()
    while not np.all(board == False):
        iter_counter += 1
        print_board(board)
        board = life_step(board)
        if iter_counter >= MAX_ITER:
            break
    print_board(board)
    print(f"wait {RESTART_DELAY}")
