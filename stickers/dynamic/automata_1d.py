#!/bin/python

import numpy as np

DELAY = 5

def get_next_state(nb_l, me, nb_r, rule):
    index = (nb_l << 2) + (me << 1) + nb_r
    return rule[index]

def get_next_generation(cur_gen, rule):
    next_gen = []
    for i in range(len(cur_gen)):
        if i + 1 >= len(cur_gen):
            nb_r = cur_gen[0]
        else:
            nb_r = cur_gen[i + 1]
        next_gen.append(get_next_state(cur_gen[i - 1], cur_gen[i], nb_r, rule))
    return next_gen

def get_random_list():
    return np.random.randint(0, 2, 8)

def draw_board(ini_gen, rule):
    gen = ini_gen
    for i in range(8):
        gen_string = [str(cell) for cell in gen]
        print(f"r{i} b{''.join(gen_string)}")
        print("update")
        gen = get_next_generation(gen, rule)

while True:
    ini_gen = get_random_list()
    rule = get_random_list()
    draw_board(ini_gen, rule)
    print(f"wait {DELAY}")
    print("clear")
    print("update")
