#include <stdio.h>
#include "mcp2210.h"
#include "max7219.h"
#include "ledmatrix.h"

/* Row from 0 to 7, counted top to bottom. Column Leds from 0x00 to 0xff. LSB is
 * the rightmost one.*/
static uint8_t screen[ROWS] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
/* Track which rows have been changed since last update to send only them on
 * next update*/
static uint8_t dirty_rows[ROWS] = { 1, 1, 1, 1, 1, 1, 1, 1 };

int init_ledmatrix(unsigned int brightness)
{
    int ret;
    ret = init_max7219();
    if (ret)
        return ret;

    enable_rows();
    clear_screen();
    update_matrix();
    set_display_test(0);
    set_brightness(brightness);
    set_decode_off();
    set_shutdown(0);

    return 0;
}

int write_row(uint8_t row, uint8_t value)
{
    if (row >= ROWS)
        return 1;

    if (screen[row] == value)
        return 0;

    dirty_rows[row] = 1;
    screen[row] = value;

    return 0;
}

int write_column(uint8_t column, uint8_t value)
{
    int row;
    int ret;

    for (row = 0; row < ROWS; row++) {
        ret = write_pixel(row, column, value & (0b1 << row));
        if (ret)
            return ret;
    }

    return 0;
}

int write_pixel(uint8_t row, uint8_t col, int value)
{
    uint8_t new_row;

    if (row >= ROWS || col >= COLS)
        return 1;

    if (value)
        new_row = screen[row] | (0b1 << col);
    else
        new_row = screen[row] & ~(0b1 << col);

    return write_row(row, new_row);
}

int fill(uint8_t row1, uint8_t col1, uint8_t row2, uint8_t col2, int value)
{
    uint8_t temp;
    int ret;

    // Make sure that row1 and col1 are the ones with lowest coordinates
    if (row2 < row1) {
        temp = row2;
        row2 = row1;
        row1 = temp;
    }
    if (col2 < col1) {
        temp = col2;
        col2 = col1;
        col1 = temp;
    }

    //TODO Terribly inefficient. But maybe not noticible in this scale.
    for (uint8_t row = row1; row <= row2; row++) {
        for (uint8_t col = col1; col <= col2; col++) {
            ret = write_pixel(row, col, value);
            if (ret)
                return ret;
        }
    }

    return 0;
}

int shift_row(uint8_t row, int dir, int n, int wrap)
{
    uint8_t new_row;

    if (row >= ROWS)
        return 1;

    new_row = dir ? screen[row] >> n : screen[row] << n;
    if (wrap)
        new_row |= dir ? screen[row] << (COLS - n) : screen[row] >> (COLS - n);

    return write_row(row, new_row);
}

static uint8_t get_pixel(uint8_t row, uint8_t column)
{
    return (screen[row] & (0b1 << column)) >> column;
}

static uint8_t get_column(uint8_t column)
{
    uint8_t col = 0;

    int row;
    for (row = 0; row < ROWS; row++)
        col |= get_pixel(row, column) << row;

    return col;
}


int shift_column(uint8_t column, int dir, int n, int wrap)
{
    uint8_t new_col;

    if (column >= COLS)
        return 1;

    new_col = dir ? get_column(column) >> n : get_column(column) << n;
    if (wrap)
        new_col |= dir ? get_column(column) << (ROWS - n) : get_column(column) >> (ROWS - n);

    return write_column(column, new_col);
}

int shift_screen(int dir, int n, int wrap)
{
    int i;
    int ret;

    for (i = 0; i < 8; i++) {
        switch (dir) {
            case 0:
                ret = shift_row(i, 0, n, wrap);
                break;
            case 1:
                ret = shift_row(i, 1, n, wrap);
                break;
            case 2:
                ret = shift_column(i, 0, n, wrap);
                break;
            case 3:
                ret = shift_column(i, 1, n, wrap);
                break;
        }
        if (ret)
            return ret;
    }

    return 0;
}

int invert_row(uint8_t row)
{
    if (row >= ROWS)
        return 1;

    return write_row(row, ~screen[row]);
}

int invert_column(uint8_t column)
{
    if (column >= COLS)
        return 1;

    return write_column(column, ~get_column(column));
}

int invert_screen()
{
    int row;
    int ret;

    for (row = 0; row < ROWS; row++) {
        ret = invert_row(row);
        if (ret)
            return ret;
    }

    return 0;
}

int invert_pixel(uint8_t row, uint8_t column)
{
    if (row >= ROWS || column >= COLS)
        return 1;

    uint8_t pixel = get_pixel(row, column);
    if (pixel)
        return write_pixel(row, column, 0);
    else
        return write_pixel(row, column, 1);
}

int swap_rows(uint8_t row1, uint8_t row2)
{
    uint8_t tmp;
    int ret;

    if (row1 >= ROWS || row2 >= ROWS)
        return 1;

    tmp = screen[row1];
    ret = write_row(row1, screen[row2]);
    if (ret)
        return ret;
    return write_row(row2, tmp);
}

int copy_row(uint8_t src, uint8_t dest)
{
    if (src >= ROWS)
        return 1;
    return write_row(dest, screen[src]);
}

int swap_columns(uint8_t column1, uint8_t column2)
{
    uint8_t tmp;
    int ret;

    if (column1 >= COLS || column2 >= COLS)
        return 1;

    tmp = get_column(column1);
    ret = write_column(column1, get_column(column2));
    if (ret)
        return ret;
    return write_column(column2, tmp);
}

int copy_column(uint8_t src, uint8_t dest)
{
    if (src >= COLS)
        return 1;
    return write_column(dest, get_column(src));
}

int copy_pixel(uint8_t src_r, uint8_t src_c, uint8_t dest_r, uint8_t dest_c)
{
    if (src_r >= ROWS || src_c >= COLS)
        return 1;
    return write_pixel(dest_r, dest_c, get_pixel(src_r, src_c));
}

int swap_pixels(uint8_t pix1_r, uint8_t pix1_c, uint8_t pix2_r, uint8_t pix2_c)
{
    uint8_t tmp;
    int ret;

    if (pix1_r >= ROWS || pix1_c >= COLS || pix2_r >= ROWS || pix2_c >= COLS)
        return 1;
    tmp = get_pixel(pix1_r, pix1_c);
    ret = write_pixel(pix1_r, pix1_c, get_pixel(pix2_r, pix2_c));
    if (ret)
        return ret;
    return write_pixel(pix2_r, pix2_c, tmp);
}

int vflip()
{
    int ret;

    for (int i = 0; i < ROWS / 2; i++) {
        ret = swap_rows(i, ROWS - 1 - i);
        if (ret)
            return ret;
    }
    return 0;
}

int hflip()
{
    int ret;

    for (int i = 0; i < COLS / 2; i++) {
        ret = swap_columns(i, COLS - 1 - i);
        if (ret)
            return ret;
    }
    return 0;
}

void update_matrix()
{
    uint8_t cline[2];
    int row;

    for (row = 0; row < ROWS; row++) {
        if (dirty_rows[row]) {
            cline[0] = row+1;
            cline[1] = screen[row];
            spi_write(cline, 2);

            dirty_rows[row] = 0;
        }
    }
}

int clear_screen()
{
    int row;
    int ret;

    for (row = 0; row < ROWS; row++) {
        ret = write_row(row, 0x00);
        if (ret)
            return ret;
    }
    return 0;
}
