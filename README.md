# ledsticker

## About Ledsticker

The Ledsticker project aims to make a LED matrix controllable through USB. The
objective is to be able to glue the LED matrix to the back of a notebook lid and
program it to show static or animated drawings (and can even be generated from a
program), effectively turning it into a *LED sticker*. For the project's
backstory, see my [blog post](https://nfraprado.net/post/ledsticker-my-first-holistic-sw-hw-project.html).

This is achieved through both software and hardware components. This repository
concerns itself with the software side. For the hardware side, see
https://codeberg.org/nfraprado/ledsticker-hw instead.

![Ledsticker showing stickers](ledsticker_final_resized_480.gif)

## Ledsticker software

The ledsticker program is responsible for parsing a sticker file, which will
describe what and how will be drawn on the LED matrix, and passing it through
USB so it can reach the board and take effect.

Being a command line program, the sticker file is to be passed as a parameter,
together with other optional flags that control behavior (eg setting the
brightness).

## Stickers

The file parsed by `ledsticker` to display on the LED matrix is called a
*sticker*.

Stickers can either be static or dynamic.
Static stickers are plain text files that are parsed directly by `ledsticker`.
Dynamic stickers are any program or script that outputs valid sticker commands.

Both static and dynamic stickers can have a single or multiple frames.

Static stickers are good for writing short and simple stickers.
They have to be manually typed using sticker commands, but don't require any
programming.

Dynamic stickers enable more complex behavior since the sticker commands on the
output are generated by the code (which can be written in any programming
language), allowing all the power of programming languages to be used on the
sticker.

## Installation

### Notes

To compile it, ledsticker requires libudev to be installed.
The other dependency,
[MCP2210-Library](https://github.com/kerrydwong/MCP2210-Library), is bundled on
this repository.

This program was tested **only** on Linux, although it may work on other platforms.

### Setup

After cloning the repository, run `make` to compile.

To install, run `make install` with root access (e.g. with sudo).

To run ledsticker without root access, follow the instructions on [MCP2210 udev
rules](https://github.com/kerrydwong/MCP2210-Library/blob/master/99-hid.rules).
The following udev rule worked for me after rebooting: `KERNEL=="hidraw*",
ATTRS{idVendor}=="04d8", ATTRS{idProduct}=="00de", MODE="0666"`.

## Usage

### Command-line interface

The `ledsticker` command supports the following parameters:

`-s, --static filename`: The file `filename` is open and parsed using the
sticker vocabulary.
This is the option used to run a plain text sticker.

`-d, --dynamic command`: The `command` is run, and its output is parsed using
the sticker vocabulary.
This is the option used to run a sticker generated by another program.

`-l, --loop`: Make the sticker loop, restarting at the end.
Only works with static stickers.

`-f, --fps n`: Run a sticker at `n` frames per second.
This is only the initial FPS configuration and will be overwritten by a `fps`
sticker command.

`-b, --brightness n`: Set the LED matrix brightness to `n`.
Valid values range from 0, darkest, to 15, brightest.
This is only the initial brightness configuration and will be overwritten by a
`bright` sticker command.

`-t, --test`: Set the LED matrix to display test mode, where all pixels are on
and with max brightness, and exit.
Useful to test if all LEDs are working.

`-c, --clear`: Clear the screen, turning all leds off, and exit.
Equivalent to running a sticker with only `clear` and `update`.
After running a sticker, the last frame will continue to display indefinitely.
This command can be used to clear it.

`-h, --help`: Display the help menu.

### Sticker syntax

Stickers are mainly made of sticker commands.

Blank lines are allowed for readability purposes.

Comments can be made by inserting a '#' as the first character of a line.
Inline comments are not allowed.

##### Sticker commands

`update`: Update the pixels on the LED matrix.
**Almost all commands only take effect after an update.**

`on R C`: Turn the pixel on row `R` and column `C` on.

`off R C`: Turn the pixel on row `R` and column `C` off.

`rN bBBBBBBBB`: Set the pixels on row `N` to the values specified in binary by
each `B`.
Each `B` can be 0, to turn off, or 1, to turn on.
The rightmost `B` corresponds to the rightmost pixel on the row.
Instead of the binary notation, hexadecimal can also be used by `rN xFF`.

`cN bBBBBBBBB`: Same as the previous command but for a column.
The rightmost `B` corresponds to the topmost pixel on the column.

`frame`: Set the pixels for a whole frame according to the next 8 lines.
Each line specifies a row of pixels in either binary or hexadecimal format.
The formats can also be mixed.
The command format is the following:
```
frame
bBBBBBBBB
bBBBBBBBB
bBBBBBBBB
bBBBBBBBB
bBBBBBBBB
bBBBBBBBB
bBBBBBBBB
bBBBBBBBB
```

`fill R1 C1 R2 C2`: Turn all pixels inside the rectangle delimited by rows `R1`
and `R2` and columns `C1` and `C2` on.
The limits are inclusive, so even those delimiting pixels (`R1`,`C1` and
`R2`,`C2`) will be turned on.
This can also be used to draw a vertical or horizontal line, if both rows or
both columns are the same, respectively.

`unfill R1 C1 R2 C2`: Same as `fill`, but turn off instead of on.

`rshift R N W`: Shift the pixels on row `R`, `N` times to the right.
Wrapping is controlled by `W` with 1 being on and 0, off.
When wrapping is on, the pixels shifted past the last pixel on the right wrap
around to the left.

`lshift R N W`: Same as `rshift` but shift to the left.

`ushift C N W`: Same as `rshift` but shift the column `C` upwards.

`dshift C N W`: Same as `ushift` but shift downwards.

`srshift N W`: Same as `rshift` but shift the whole screen.

`slshift N W`: Same as `srshift` but shift to the left.

`sushift N W`: Same as `srshift` but shift upwards.

`sdshift N W`: Same as `srshift` but shift downwards.

`invert R C`: Invert the pixel on row `R` and column `C`.
If it was on, turn it off, and vice versa.

`rinvert R`: Same as `invert` but for the whole row `R`.

`cinvert C`: Same as `invert` but for the whole column `C`.

`sinvert`: Same as `invert` but for the whole screen.

`vflip`: Flip the screen vertically.
The top row swaps with the bottom one and so on.

`hflip`: Flip the screen horizontally.
The leftmost column swaps with the rightmost and so on.

`copy R C S D`: Copy the pixel on row `R` and column `C` to the pixel on row `S`
and column `D`, overwriting the latter with the value of the former.

`rcopy R S`: Same as `copy` but for the whole row `R` onto the whole row `S`.

`ccopy C D`: Same as `copy` but for the whole column `C` onto the whole column
`D`.

`swap R C S D`: Swap the pixel of row `R` and column `C` with the one on row `S`
and column `D`.

`rswap R S`: Same as `swap` but for the whole rows `R` and `S`.

`cswap C D`: Same as `swap` but for the whole columns `C` and `D`.

`clear`: Clear the screen, turning all pixels off.

`soff`: Turn the screen off.
After turning the screen back on again all pixels return to their former state.

`son`: Turn the screen on.
At the beginning of a sticker, this is already automatically done, and only
needs to be done manually after a `soff`.

`wait N`: Wait for the time of `N` frames to pass by before continuing.

`fps N`: Set the frames per second to be `N`.
The minimum is 1 and there is no maximum, although high numbers (over 30) will
start to cause data transmission errors.

`bright N`: Set the brightness level of the pixels to `N`.
Can range from 0 to 15.

## Caveats

There's a bug that causes dynamic stickers to fail to load sometimes. It's
probably related to the usage of `popen()`, but I have yet to solve it.

## Credits

### Author

Nícolas F. R. A. Prado ([nfraprado](https://codeberg.org/nfraprado/))

### Libraries

[MCP2210-Library](https://github.com/kerrydwong/MCP2210-Library)
