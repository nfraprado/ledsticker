#ifndef TIMER_H__
#define TIMER_H__

/**
 * Pause the program until next SIGALRM and the update the screen
 */
void sync_update();

/**
 * Initialize the timer based on the fps
 * @fps:    frames per second to update the screen
 */
void timer_init(int fps);

/**
 * Set the fps value. Update the timer interval.
 * @fps:    frames per second
 */
int set_fps(int fps);

/**
 * Wait for a number of frames to pass
 * @count:  number of frames to wait for
 */
void wait_frames(int count);

#endif
