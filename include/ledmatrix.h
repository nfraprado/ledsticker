#ifndef LEDMATRIX_H_
#define LEDMATRIX_H_

#include <stdint.h>

/*The led matrx has 8 rows and 8 columns*/
#define ROWS 8
#define COLS 8

/**
 * Initialize LED matrix
 * @brightness: initial value of LED brightness to be set
 */
int init_ledmatrix(unsigned int brightness);

/**
 * Write value to LEDs of a row
 * @row:    row number to write to
 * @value:  binary value for each LED on the row
 */
int write_row(uint8_t row, uint8_t value);

/**
 * Write value to LEDs of a column
 * @column: column number to write to
 * @value:  binary value for each LED on the column
 */
int write_column(uint8_t column, uint8_t value);

/**
 * Write value to a single LED
 * @row:    row of the pixel to write to
 * @col:    column of the pixel to write to
 * @value:  value to write, 1 to turn on, 0 to turn off
 */
int write_pixel(uint8_t row, uint8_t col, int value);

/**
 * Write value to all LEDs inside the rectangle with upper right edge at
 * (row1,col1) and lower left edge at (row2,col2).
 * @row1:    row of the first edge
 * @col1:    column of the first edge
 * @row2:    row of the second edge
 * @col2:    column of the second edge
 * @value:  value to write, 1 to turn on, 0 to turn off
 */
int fill(uint8_t row1, uint8_t col1, uint8_t row2, uint8_t col2, int value);

/**
 * Shift the LED values on a row
 * @row:    row to shift
 * @dir:    direction of the shift. 1 to shift to the right, 0 to the left.
 * @n:      number of bits to shift
 * @wrap:   whether to wrap on the other side. 1 to wrap, 0 to not wrap.
 */
int shift_row(uint8_t row, int dir, int n, int wrap);

/**
 * Shift the LED values on a column
 * @column: column to shift
 * @dir:    direction of the shift. 1 to shift upwards, 0 downwards.
 * @n:      number of bits to shift
 * @wrap:   whether to wrap on the other side. 1 to wrap, 0 to not wrap.
 */
int shift_column(uint8_t column, int dir, int n, int wrap);

/**
 * Shift the LED values of the whole screen
 * @dir:    direction of the shift. 0 to shift to the left, 1 right, 2 downwards
 *          and 3 upwwards
 * @n:      number of bits to shift
 * @wrap:   whether to wrap on the other side. 1 to wrap, 0 to not wrap.
 */
int shift_screen(int dir, int n, int wrap);

/**
 * Invert LED values on a row
 * @row:    row to invert
 */
int invert_row(uint8_t row);

/**
 * Invert LED values on a column
 * @column: column to invert
 */
int invert_column(uint8_t column);

/**
 * Invert LED values of the whole screen
 */
int invert_screen();

/**
 * Invert the value of a single pixel
 * @row:    row of the pixel
 * @column: column of the pixel
 */
int invert_pixel(uint8_t row, uint8_t column);

/**
 * Copy value of a pixel to another one
 * @src_r:  row of the source pixel
 * @src_c:  column of the source pixel
 * @dest_r: row of the destination pixel
 * @dest_c: column of the destination pixel
 */
int copy_pixel(uint8_t src_r, uint8_t src_c, uint8_t dest_r, uint8_t dest_c);

/**
 * Swap the value of two pixels
 * @pix1_r: row of the first pixel
 * @pix1_c: column of the first pixel
 * @pix2_r: row of the second pixel
 * @pix2_c: column of the second pixel
 */
int swap_pixels(uint8_t pix1_r, uint8_t pix1_c, uint8_t pix2_r, uint8_t pix2_c);

/**
 * Swap the value of two rows
 * @row1:   number of the first row
 * @row2:   number of the second row
 */
int swap_rows(uint8_t row1, uint8_t row2);

/**
 * Copy the value from one row to another
 * @src:    number of the source row
 * @dest:   number of the destination row
 */
int copy_row(uint8_t src, uint8_t dest);

/**
 * Swap the value of two columns
 * @column1:    number of the first column
 * @column2:    number of the second column
 */
int swap_columns(uint8_t column1, uint8_t column2);

/**
 * Copy the value from one column to another
 * @src:    number of the source column
 * @dest:   number of the destination column
 */
int copy_column(uint8_t src, uint8_t dest);

/**
 * Flip the screen vertically. Along the horizontal central axis
 */
int vflip();

/**
 * Flip the screen horizontally. Along the vertical central axis
 */
int hflip();

/**
 * Update the LEDs on the ledmatrix to the values on the internal buffer.
 */
void update_matrix();

/**
 * Clear the screen
 */
int clear_screen();

#endif
