#include <getopt.h>
#include "ledmatrix.h"
#include "max7219.h"
#include "sticker_parser.h"
#include "timer.h"

#define DEFAULT_FPS 5
#define DEFAULT_BRIGHTNESS 3

/*
 * Print the help message
 * prog_name     - the name used to invoke the program
 */
void print_help(char *prog_name)
{
    fprintf(stderr, "Usage: %s [options]\n\n", prog_name);
    fprintf(stderr, "  Options:\n"
            "    -s, --static filename  display 'filename' static sticker\n"
            "    -d, --dynamic command  display 'command' dynamic sticker\n"
            "    -l, --loop             loop at the end of the sticker\n"
            "    -f, --fps n            set 'n' frames per second. Minimum is 1\n"
            "    -b, --brightness n     set 'n' brightness. Minimum is 0, maximum is 15\n"
            "    -t, --test             enable matrix test mode and exit\n"
            "    -c, --clear            clear matrix screen and exit\n"
            "    -h, --help             print this help message and exit\n");
}

int main(int argc, char** argv)
{
    enum sticker_type type;
    char *name;
    int loop = 0;
    unsigned int fps = DEFAULT_FPS;
    unsigned int brightness = DEFAULT_BRIGHTNESS;
    unsigned int test_display = 0;
    unsigned int clear_display = 0;
    int sticker_type_set = 0;

    static struct option long_options[] = {
        {"static",      required_argument,  0,  's'},
        {"loop",        no_argument,        0,  'l'},
        {"dynamic",     required_argument,  0,  'd'},
        {"fps",         required_argument,  0,  'f'},
        {"brightness",  required_argument,  0,  'b'},
        {"test",        no_argument,        0,  't'},
        {"clear",       no_argument,        0,  'c'},
        {"help",        no_argument,        0,  'h'},
        {0,             0,                  0,   0}
    };

    int option_index = 0;
    int opt;

    while((opt = getopt_long(argc, argv, "hls:d:f:b:ct", long_options, &option_index)) != -1){
        switch(opt){
            case 's':
                type = STATIC_STICKER;
                name = strdup(optarg);
                sticker_type_set = 1;
                break;
            case 'l':
                loop = 1;
                break;
            case 'd':
                type = DYNAMIC_STICKER;
                name = strdup(optarg);
                sticker_type_set = 1;
                break;
            case 'f':
                fps = atoi(optarg);
                break;
            case 'b':
                brightness = atoi(optarg);
                break;
            case 't':
                test_display = 1;
                sticker_type_set = 1;
                break;
            case 'c':
                clear_display = 1;
                sticker_type_set = 1;
                break;
            case 'h':
                print_help(argv[0]);
                exit(0);
                break;
            default:
                exit(1);
        }
    }

    if (type == DYNAMIC_STICKER && loop) {
        printf("Error: Dynamic stickers can't be looped\n");
        exit(1);
    }
    if (!sticker_type_set) {
        printf("Error: No sticker type defined\n");
        exit(1);
    }

    if (brightness > 15) {
        printf("Warning: Invalid brightness value %d. Should be between 0 and"
               " 15. Using default value %d.\n", brightness,
               DEFAULT_BRIGHTNESS);
        brightness = DEFAULT_BRIGHTNESS;
    }

    int ret = init_ledmatrix(brightness);
    if (ret) {
        printf("Error: Unable to initialize led matrix. Is the board connected?\n");
        return ret;
    }

    if (test_display) {
        set_display_test(1);
    }
    else if (clear_display) {
        clear_screen();
        update_matrix();
    }
    else {
        // Normal operation. Run sticker

        if (fps == 0) {
            printf("Warning: Invalid fps value %d. Should be 1 or greater"
                   " (and preferably less than 30). Using default value %d.\n",
                   fps, DEFAULT_FPS);
            fps = DEFAULT_FPS;
        }
        else if (fps > 30) {
            printf("Warning: Fps value is greater than 30, which can cause"
                   " transmission errors. For best results keep it below"
                   " 30.\n");
        }
        timer_init(fps);
        parse_sticker(name, type, loop);
    }

    release_max7219();

    return 0;
}
