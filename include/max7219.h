#ifndef MAX7219_H_
#define MAX7219_H_

#include <stdint.h>
#include "mcp2210.h"

/**
 * Initialize MAX7219
 */
int init_max7219();

/**
 * Write data to MAX7219 through SPI interface
 * @data:   array to write from
 * @size:   number of bytes to write
 */
void spi_write(uint8_t* data, uint8_t size);

/**
 * Set shutdown or normal mode operation
 * @on:   0 for normal mode, 1 for shutdown mode
 */
void set_shutdown(int on);

/**
 * Enable all rows to be shown
 */
void enable_rows();

/**
 * Set brightness level of display
 * @bright: value from 0x0 to 0xf
 */
void set_brightness(uint8_t bright);

/**
 * Set display test mode
 * @on: 1 for test mode, 0 for normal mode
 */
void set_display_test(int on);

/**
 * Set decode mode off. Decode mode isn't useful for LED matrices
 */
void set_decode_off();

/**
 * Release MAX7219 resources
 */
void release_max7219();

#endif
