#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include "ledmatrix.h"
#include "timer.h"

static struct itimerval timer;
static struct sigaction sa;

static void sig_handler(int sig)
{
    //do nothing since the SIGALRM signal already wakes the program up from pause()
}

static void enable_timer()
{
    sigaction(SIGALRM, &sa, NULL);
}

static void disable_timer()
{
    signal(SIGALRM, SIG_IGN);
}

void wait_frames(int count)
{
    enable_timer();
    while(count){
        //block the program until next SIGALRM
        pause();
        count--;
    }
    disable_timer();
}

void sync_update()
{
    enable_timer();
    pause();
    disable_timer();
    update_matrix();
}

void timer_init(int fps)
{
    //register sig_handler as the callback for the SIGALRM
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sig_handler;

    disable_timer();

    set_fps(fps);
}

int set_fps(int fps)
{
    if (fps < 1){
        fprintf(stderr, "Invalid fps count '%d'. Must be at least 1.\n", fps);
        return 1;
    }

    if (fps > 1) {
        int usec = 1000000/fps;

        //time from now
        timer.it_value.tv_sec = 0;
        timer.it_value.tv_usec = usec;
        //time after each expiration
        timer.it_interval.tv_sec = 0;
        timer.it_interval.tv_usec = usec;
    }
    else {
        //time from now
        timer.it_value.tv_sec = 1;
        timer.it_value.tv_usec = 0;
        //time after each expiration
        timer.it_interval.tv_sec = 1;
        timer.it_interval.tv_usec = 0;
    }

    //set timer
    return setitimer(ITIMER_REAL, &timer, NULL);
}
