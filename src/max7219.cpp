#include "max7219.h"

/*Based on MAX7219 datasheet*/
static uint8_t cmd_shutdown_off[] = {0x0c, 0x01};
static uint8_t cmd_shutdown_on[] = {0x0c, 0x00};
static uint8_t cmd_show[] = {0x0b, 0x07};
static uint8_t cmd_test_off[] = {0x0f, 0x00};
static uint8_t cmd_test_on[] = {0x0f, 0x01};
static uint8_t cmd_brightness[] = {0x0a, 0x00};
static uint8_t cmd_decode_off[] = {0x09, 0x00};

#define CS_PIN 0

//Objects to handle the MCP2210
static hid_device *mcp_handle;
static GPPinDef mcp_pins;

static int set_cs(int level);

int init_max7219()
{
    //Initializing the MCP2210 device
    mcp_handle = InitMCP2210();
    if (mcp_handle == 0)
    {
        //Error initializing
        return 1;
    }

    /**
     * Configure CS_PIN direction to output
     */
    mcp_pins = GetGPIOPinDirection(mcp_handle);
    mcp_pins.GP[CS_PIN].GPIODirection = GPIO_DIRECTION_OUTPUT;
    int r = SetGPIOPinDirection(mcp_handle, mcp_pins);

    //Set CS as HIGH
    set_cs(1);

    return r;
}

void spi_write(uint8_t* data, uint8_t size)
{
    SPIDataTransferStatusDef status;

    set_cs(0);
    //Repeat sending until successful
    do {
        status = SPIDataTransfer(mcp_handle, data, size);
    } while (status.SPIEngineStatus == 0x10);

    if (status.ErrorCode)
        printf("Error %d while transferring\n", status.ErrorCode);

    //Latches last 16 bits on the MAX7219 shift register
    set_cs(1);
}

void set_shutdown(int on)
{
    if (on)
        spi_write(cmd_shutdown_on, 2);
    else
        spi_write(cmd_shutdown_off, 2);
}

void enable_rows()
{
    spi_write(cmd_show, 2);
}

void set_brightness(uint8_t bright)
{
    cmd_brightness[1] = bright;
    spi_write(cmd_brightness, 2);
}

void set_display_test(int on)
{
    if (on)
        spi_write(cmd_test_on, 2);
    else
        spi_write(cmd_test_off, 2);
}
void set_decode_off()
{
    spi_write(cmd_decode_off, 2);
}

static int set_cs(int level)
{
    mcp_pins.GP[CS_PIN].GPIOOutput = level;
    return SetGPIOPinVal(mcp_handle, mcp_pins);
}

void release_max7219()
{
    ReleaseMCP2210(mcp_handle);
}
