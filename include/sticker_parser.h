#ifndef STICKER_PARSER_H__
#define STICKER_PARSER_H__

enum sticker_type {STATIC_STICKER, DYNAMIC_STICKER};

/**
 * Parse the sticker.
 * @filename:   name of the file of the static sticker or command to be run for
 *              dynamic sticker.
 * @type:       type of the sticker
 * @loop:       whether to restart the sticker at the end or not. Only for
 *              static sticker
 */
void parse_sticker(char *filename, enum sticker_type type, int loop);

/**
 * Parse sticker command
 * @line:   line containing the current command
 * @len:    length of the line
 * @fp:     pointer to the file/pipe of the sticker
 */
int parse_command(char *line, size_t len, FILE *fp);

/**
 * Convert string of 0s and 1s to binary
 * @str:    string to convert
 */
unsigned int atob(char *str);

#endif
