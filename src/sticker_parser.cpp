#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sticker_parser.h"
#include "ledmatrix.h"
#include "max7219.h"
#include "timer.h"

#define MAXCMDSZ 20

char failed_cmd[MAXCMDSZ] = "";

void parse_sticker(char *filename, enum sticker_type type, int loop)
{
    char *line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    FILE *fp;
    int sticker_error = 0;
    int line_num = 0;

    if (type == DYNAMIC_STICKER)
        //run process and open pipe for the output of the dynamic sticker
        fp = popen(filename, "r");
    else
        //open file to read the static sticker
        fp = fopen(filename, "r");

    if (fp == NULL){
        fprintf(stderr, "Error while parsing sticker file: Unable to open file '%s'.\n", filename);
        exit(1);
    }

    while (!sticker_error) {
        while ((read = getline(&line, &len, fp)) != -1){
            line_num++;
            //skip empty lines and comment lines
            if (len > 1 && line[0] != '#')
                if (parse_command(line, len, fp)) {
                    printf("Error while parsing sticker command '%s' at line %d\n", failed_cmd, line_num);
                    sticker_error = 1;
                    break;
                }
        }
        if (loop)
            rewind(fp);
        else
            break;
    }

    if (type == DYNAMIC_STICKER)
        pclose(fp);
    else
        fclose(fp);
    free(line);
}

int parse_command(char *line, size_t len, FILE *fp)
{
    char command[MAXCMDSZ];
    if (sscanf(line, "%20s", command) != 1)
        return 0;

    strcpy(failed_cmd, command);

    //get row command
    if(command[2] == '\0' && command[0] == 'r' && command[1] >= '0' && command[1] <= '7'){
        unsigned int value;
        char binary_str[COLS + 1];
        int ret;
        if (sscanf(line, "r%*[0-9] x%2x", &value) != 1) {
            ret = sscanf(line, "r%*[0-9] b%8s", binary_str);
            if (ret != 1 || (binary_str[0] != '0' && binary_str[0] != '1'))
                return 1;
            value = atob(binary_str);
        }
        return write_row(command[1] - '0', (uint8_t) value);
    }
    //get column command
    if(command[2] == '\0' && command[0] == 'c' && command[1] >= '0' && command[1] <= '7'){
        unsigned int value;
        char binary_str[COLS + 1];
        int ret;
        if (sscanf(line, "c%*[0-9] x%2x", &value) != 1) {
            ret = sscanf(line, "c%*[0-9] b%8s", binary_str);
            if (ret != 1 || (binary_str[0] != '0' && binary_str[0] != '1'))
                return 1;
            value = atob(binary_str);
        }
        return write_column(command[1] - '0', (uint8_t) value);
    }
    if (strncmp(command, "rshift", MAXCMDSZ) == 0) {
        int row = 0, n = 0, wrap = 0;
        if (sscanf(line, "rshift %1d %1d %1d", &row, &n, &wrap) != 3)
            return 1;
        return shift_row(row, 1, n, 0);
    }
    if (strncmp(command, "lshift", MAXCMDSZ) == 0) {
        int row = 0, n = 0, wrap = 0;
        if (sscanf(line, "lshift %1d %1d %1d", &row, &n, &wrap) != 3)
            return 1;
        return shift_row(row, 0, n, 0);
    }
    if (strncmp(command, "ushift", MAXCMDSZ) == 0) {
        int col = 0, n = 0, wrap = 0;
        if (sscanf(line, "ushift %1d %1d %1d", &col, &n, &wrap) != 3)
            return 1;
        return shift_column(col, 1, n, wrap);
    }
    if (strncmp(command, "dshift", MAXCMDSZ) == 0) {
        int col = 0, n = 0, wrap = 0;
        if (sscanf(line, "dshift %1d %1d %1d", &col, &n, &wrap) != 3)
            return 1;
        return shift_column(col, 0, n, wrap);
    }
    if (strncmp(command, "srshift", MAXCMDSZ) == 0) {
        int n = 0, wrap = 0;
        if (sscanf(line, "srshift %1d %1d", &n, &wrap) != 2)
            return 1;
        return shift_screen(1, n, wrap);
    }
    if (strncmp(command, "slshift", MAXCMDSZ) == 0) {
        int n = 0, wrap = 0;
        if (sscanf(line, "slshift %1d %1d", &n, &wrap) != 2)
            return 1;
        return shift_screen(0, n, wrap);
    }
    if (strncmp(command, "sushift", MAXCMDSZ) == 0) {
        int n = 0, wrap = 0;
        if (sscanf(line, "sushift %1d %1d", &n, &wrap) != 2)
            return 1;
        return shift_screen(3, n, wrap);
    }
    if (strncmp(command, "sdshift", MAXCMDSZ) == 0) {
        int n = 0, wrap = 0;
        if (sscanf(line, "sdshift %1d %1d", &n, &wrap) != 2)
            return 1;
        return shift_screen(2, n, wrap);
    }
    if (strncmp(command, "rinvert", MAXCMDSZ) == 0) {
        int row = 0;
        if (sscanf(line, "rinvert %1d", &row) != 1)
            return 1;
        return invert_row(row);
    }
    if (strncmp(command, "cinvert", MAXCMDSZ) == 0) {
        int column = 0;
        if (sscanf(line, "cinvert %1d", &column) != 1)
            return 1;
        return invert_column(column);
    }
    if (strncmp(command, "sinvert", MAXCMDSZ) == 0) {
        return invert_screen();
    }
    if (strncmp(command, "invert", MAXCMDSZ) == 0) {
        int row = 0, column = 0;
        if (sscanf(line, "invert %1d %1d", &row, &column) != 2)
            return 1;
        return invert_pixel(row, column);
    }
    if (strncmp(command, "rswap", MAXCMDSZ) == 0) {
        int row1 = 0, row2 = 0;
        if (sscanf(line, "rswap %1d %1d", &row1, &row2) != 2)
            return 1;
        return swap_rows(row1, row2);
    }
    if (strncmp(command, "cswap", MAXCMDSZ) == 0) {
        int col1 = 0, col2 = 0;
        if (sscanf(line, "cswap %1d %1d", &col1, &col2) != 2)
            return 1;
        return swap_columns(col1, col2);
    }
    if (strncmp(command, "vflip", MAXCMDSZ) == 0)
        return vflip();
    if (strncmp(command, "hflip", MAXCMDSZ) == 0)
        return hflip();
    if (strncmp(command, "rcopy", MAXCMDSZ) == 0) {
        int src = 0, dest = 0;
        sscanf(line, "rcopy %1d %1d", &src, &dest);
        return copy_row(src, dest);
    }
    if (strncmp(command, "ccopy", MAXCMDSZ) == 0) {
        int src = 0, dest = 0;
        sscanf(line, "ccopy %1d %1d", &src, &dest);
        return copy_column(src, dest);
    }
    if (strncmp(command, "copy", MAXCMDSZ) == 0) {
        int src_r = 0, src_c = 0, dest_r = 0, dest_c = 0;
        sscanf(line, "copy %1d %1d %1d %1d", &src_r, &src_c, &dest_r, &dest_c);
        return copy_pixel(src_r, src_c, dest_r, dest_c);
    }
    if (strncmp(command, "swap", MAXCMDSZ) == 0) {
        int pix1_r = 0, pix1_c = 0, pix2_r = 0, pix2_c = 0;
        sscanf(line, "swap %1d %1d %1d %1d", &pix1_r, &pix1_c, &pix2_r, &pix2_c);
        return swap_pixels(pix1_r, pix1_c, pix2_r, pix2_c);
    }
    if (strncmp(command, "update", MAXCMDSZ) == 0) {
        sync_update();
        return 0;
    }
    if (strncmp(command, "fps", MAXCMDSZ) == 0){
        unsigned int fps = 0;
        sscanf(line, "fps %2d", &fps);
        return set_fps(fps);
    }
    if (strncmp(command, "bright", MAXCMDSZ) == 0){
        unsigned int bright = 0;
        sscanf(line, "bright %2d", &bright);
        if (bright <= 15)
            set_brightness(bright);
        else {
            fprintf(stderr, "Error while parsing sticker file: Invalid value for brightness '%d'.\n", bright);
            exit(1);
        }
    }
    if (strncmp(command, "soff", MAXCMDSZ) == 0) {
        set_shutdown(1);
        return 0;
    }
    if (strncmp(command, "son", MAXCMDSZ) == 0) {
        set_shutdown(0);
        return 0;
    }
    if (strncmp(command, "frame", MAXCMDSZ) == 0){
        int ret;
        for (int row = 0; row < ROWS; row++){
            if(getline(&line, &len, fp) == -1)
                return 1;
            unsigned int value;
            char binary_str[COLS + 1];
            if (sscanf(line, "x%2x", &value) != 1) {
                if (sscanf(line, "b%8s", binary_str) != 1)
                    return 1;
                value = atob(binary_str);
            }
            ret = write_row(row, (uint8_t) value);
            if (ret)
                return ret;
        }
        return 0;
    }
    if (strncmp(command, "clear", MAXCMDSZ) == 0)
        return clear_screen();
    if (strncmp(command, "wait", MAXCMDSZ) == 0){
        int count;
        if (sscanf(line, "wait %2d", &count) != 1)
            return 1;
        wait_frames(count);
        return 0;
    }
    if (strncmp(command, "on", MAXCMDSZ) == 0){
        int row, col;
        if (sscanf(line, "on %1d %1d", &row, &col) != 2)
            return 1;

        return write_pixel(row, col, 1);
    }
    if (strncmp(command, "off", MAXCMDSZ) == 0){
        int row, col;
        if (sscanf(line, "off %1d %1d", &row, &col) != 2)
            return 1;
        return write_pixel(row, col, 0);
    }
    if (strncmp(command, "fill", MAXCMDSZ) == 0){
        int row1, col1, row2, col2;
        if (sscanf(line, "fill %1d %1d %1d %1d", &row1, &col1, &row2, &col2) != 4)
            return 1;
        return fill(row1, col1, row2, col2, 1);
    }
    if (strncmp(command, "unfill", MAXCMDSZ) == 0){
        int row1, col1, row2, col2;
        if (sscanf(line, "unfill %1d %1d %1d %1d", &row1, &col1, &row2, &col2) != 4)
            return 1;
        return fill(row1, col1, row2, col2, 0);
    }

    printf("Error: Unknown sticker command '%s'\n", command);
    return 1;
}

unsigned int atob(char *str)
{
    int binary = 0;
    while(*str == '0' || *str == '1'){
        binary += *str - '0';
        binary <<= 1;
        str++;
    }
    binary >>= 1;
    return binary;
}
