OUT = ledsticker

SRCDIR = src
INCDIR = include

MCP_DIR = libs/mcp2210
MCP_SRCDIR = $(MCP_DIR)/src
MCP_INCDIR = $(MCP_DIR)/include

PREFIX ?= /usr/local

CC=gcc
CXX=g++
CFLAGS = -Wall -g -Wextra -I$(INCDIR) -I$(MCP_INCDIR)
CXXFLAGS = $(CFLAGS)

LIBS=-ludev

CSRC = $(wildcard $(SRCDIR)/*.c)
CPPSRC = $(wildcard $(SRCDIR)/*.cpp)

OBJ = $(CSRC:.c=.o) $(CPPSRC:.cpp=.o) $(MCP_SRCDIR)/mcp2210.o

$(OUT): $(OBJ) $(MCP_SRCDIR)/hid.o
	$(CXX) -o $@ $^ $(CFLAGS) $(LIBS)

$(OBJ): %.o: %.cpp
	$(CXX) -o $@ -c $< $(CFLAGS)

# Compile hid.c separately from others because needs to be compiled with CC
$(MCP_SRCDIR)/hid.o: %.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

install: $(OUT)
	install -D -m755 $(OUT) ${DESTDIR}${PREFIX}/bin/$(OUT)

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	rm -f ${DESTDIR}${PREFIX}/bin/$(OUT)

tags: $(CSRC) $(CPPSRC)
	ctags $(CSRC) $(CPPSRC) $(wildcard $(INCDIR)/*.h)

.PHONY: clean

clean:
	rm -f $(OBJ) $(OUT) $(MCP_SRCDIR)/hid.o tags
